



 
# 万岳外卖配送系统


[![](https://img.shields.io/badge/%E9%83%A8%E7%BD%B2%E6%96%87%E6%A1%A3-%E7%82%B9%E5%87%BB%E6%9F%A5%E7%9C%8B-yellow)](https://www.kancloud.cn/wanyuekaiyuan11/wanyue_waimai/3130565)
[![](https://img.shields.io/badge/QQ%E7%BE%A4-995910672-green)](https://qm.qq.com/cgi-bin/qm/qr?k=JShAyXeoKqg2lWFEUSElxELImhjeMG4y&jump_from=webapi)


### 【20230518更新说明】重新提交swoole文件夹下的代码文件，解决之前出现的缺失文件问题

### uniapp代码下载地址
- uniapp仓库地址: <a target="_blank" href="https://gitee.com/WanYueKeJi/wanyue_waimai_uniapp">点击此处</a>

### 项目说明及部署文档（如果对你有用，请给个star！）
##### <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/wanyue_waimai/3130565">项目文档</a> |  <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/wanyue_waimai/3130565">部署文档</a> |  <a target="_blank" href="http://qm.qq.com/cgi-bin/qm/qr?k=JShAyXeoKqg2lWFEUSElxELImhjeMG4y&jump_from=webapi">帮助与咨询</a>

---
### 系统演示

- 总后台地址: https://takeout.sdwanyue.com/admin    演示账号：demo  演示密码：123456
- 分站后台地址: https://takeout.sdwanyue.com/substation     演示账号：quanqiu  演示密码：qwe123
- 用户端演示账号：17188888886   验证码123456
- 骑手端演示账号：15599999999   验证码123456
- 商家端演示账号：13311111111   验证码123456


   
### 项目介绍
万岳同城外卖系统,系统包含商家端、配送端、用户端以及总管理后台、城市配送后台，在线下单，商家接单，骑手抢单配送
* 所有使用到的框架或者组件都是基于开源项目,代码保证100%开源。
* 系统功能通用，无论是个人还是企业都可以利用该系统快速搭建一个属于自己的外卖服务平台。


### 技术亮点
```
    1.后台应用ThinkCMF快速生成现代化表单； 
    2.PHPExcel数据导出,导出表格更加美观,可视；
    3.支持微信/支付宝支付,支付接入更加快捷,简单；
    4.后台多任务窗口化操作界面；
    5.内置强大灵活的权限管理；
    6.内置组合数据,系统配置,管理碎片化数据；
    7.客户端完善的交互效果和动画；
    8.高频数据缓存； 
    9.内置PhalApi接口框架,前后端分离更方便；
    10.WebSocket长连接减少CPU及内存使用及网络堵塞，减少请求响应时长；
    11.支持队列降低流量高峰，解除耦合，高可用;
    12.无需安装, clone下来即可直接使用, 完全100%真开源；
```

### 系统演示
![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_waimai_web/raw/master/images/1.png)
![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_waimai_web/raw/master/images/2.png)
![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_waimai_web/raw/master/images/3.png)
![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_waimai_web/raw/master/images/4.png)
![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_waimai_web/raw/master/images/5.png)
![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_waimai_web/raw/master/images/6.png)
![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_waimai_web/raw/master/images/7.png)
![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_waimai_web/raw/master/images/8.png)





  ###  技术交流群【获取sql脚本,加群请填写“gitee外卖”】

群名称:万岳科技开源讨论10群
[QQ群:995910672【点击可直接跳转】](http://qm.qq.com/cgi-bin/qm/qr?k=JShAyXeoKqg2lWFEUSElxELImhjeMG4y&jump_from=webapi)

群名称:万岳科技开源讨论15群
[QQ群:681418688【点击可直接跳转】](http://qm.qq.com/cgi-bin/qm/qr?k=JShAyXeoKqg2lWFEUSElxELImhjeMG4y&jump_from=webapi)

